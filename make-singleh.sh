#!/bin/sh

set -e
set -x

echo "
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>
#include <math.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
" > all.h

cat src/stb_*.h >> all.h
cat src/utf8.h | grep -v "#include" >> all.h
cat src/libcaptcha.h | grep -v "#include" >> all.h
cat src/global.h | grep -v "#include" >> all.h
cat src/utf8.c | grep -v "#include" > all.c
cat src/libcaptcha.c | grep -v "#include" >> all.c
cat src/utils.c | grep -v "#include" >> all.c
cat src/font.c | grep -v "#include" >> all.c
cat src/generator1.c | grep -v "#include" >> all.c
cat src/transform.c | grep -v "#include" >> all.c
cat src/image.c | grep -v "#include" >> all.c
cat src/pattern.c | grep -v "#include" >> all.c
cat src/pattern-gen.c | grep -v "#include" >> all.c
cat src/preset0.c | grep -v "#include" >> all.c
cat src/string.c | grep -v "#include" >> all.c

mkdir -p singleh
cat all.h all.c > singleh/libcaptcha.h

rm all.h
rm all.c

cd examples
cc example1.c -I../singleh -lm -o shtest
./shtest
rm shtest


echo "singleh/libcaptcha.h is ready"

