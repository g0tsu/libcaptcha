#include <stdio.h>
#include <libcaptcha.h>

/*
 * Generate screenshot using a new preset functions.
 */

int main() {
  lc_bmp * text = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 50, 0, 10, 10);
  lc_bmp * bmp = lc_create_3ch_bmp(text->w * 3 + 3, text->h * 2 + 2); //6 images
  int ow = text->w;
  int oh = text->h;

  lc_bmp * square = lc_preset_square(text, 1, 2);
  lc_merge_3ch_bmp(bmp, square, 0, 0);
  lc_free(square);

  square = lc_preset_square(text, 2, 15);
  lc_merge_3ch_bmp(bmp, square, text->w + 1, 0);
  lc_free(square);

  square = lc_preset_square(text, 2, 20);
  lc_merge_3ch_bmp(bmp, square, text->w * 2 + 1, 0);
  lc_free(square);

  lc_bmp * noise = lc_preset_noise(text);
  lc_merge_3ch_bmp(bmp, noise, 0, text->h + 1);
  lc_free(noise);

  square = lc_preset_circle(text, 0, 14);
  lc_merge_3ch_bmp(bmp, square, ow + 1, oh + 1);
  lc_free(square);

  square = lc_preset_circle(text, 0, 30);
  lc_merge_3ch_bmp(bmp, square, ow * 2 + 1, oh+ 1);
  lc_free(square);

  lc_save_png("example7.png", bmp);

  lc_free(bmp);
  lc_free(text);
  return 0;
}
