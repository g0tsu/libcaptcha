#include <stdio.h>
#include <libcaptcha.h>

/*
 * Lets apply some pattern from png image to text
 */
lc_bmp *load_pattern(char *filename, int x, int y, int w, int h) {
  lc_bmp *img = lc_load_png(filename);
  lc_bmp *cropped;

  if (!img) {
    perror("lc_load_png()");
    return NULL;
  }

  cropped = lc_crop_bmp(img, x, y, w, h);

  if (!cropped) {
    perror("lc_crop_bmp()");
    return NULL;
  }

  lc_free(img);

  return cropped;
}

int main() {
  char * str = "usepattern";
  char * fontfile = "../ttf/dejavu.ttf";

  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;
  lc_bmp * text;
  lc_bmp * pattern;
  lc_bmp * bmp;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  arr = lc_str_to_arr(font, str, 38, 0);

  if (!arr) {
    perror("lc_str_to_arr()");
    return 1;
  }

  text = lc_arr_to_bmp(arr);

  if (!text) {
    perror("lc_arr_to_bmp()");
    return 1;
  }

  pattern = load_pattern("../patterns/96b.png", 300, 20, 240, 80);
  /* looks great */
/*  lc_save_png("./example4.png", bmp);*/

  if (!pattern) {
    return 1;
  }

  bmp = lc_pattern_apply(pattern, text);

  if (!bmp) {
    perror("lc_pattern_apply()");
    return 1;
  }


  lc_save_png("./example4.png", bmp);

  lc_free(text);
  lc_free(pattern);
  lc_free(arr);
  lc_free(bmp);
  lc_free(font);
  return 0;
}
