#include <stdio.h>
#include <libcaptcha.h>

/*
 * Automatically randomize size for glyphs in the array
 */

int main() {
  char * str = "sizemetters";
  char * fontfile = "../ttf/dejavu.ttf";

  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;
  lc_bmp * bmp;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  /*
   * 38 - 80 - any random size between the range
   */
  arr = lc_str_to_arr(font, str, 38, 80);

  if (!arr) {
    perror("lc_str_to_arr()");
    return 1;
  }

  bmp = lc_arr_to_bmp(arr);

  if (!bmp) {
    perror("lc_arr_to_bmp()");
    return 1;
  }

  lc_save_png("./example2.png", bmp);

  lc_free(arr);
  lc_free(bmp);
  lc_free(font);
  return 0;
}
