/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

lc_bmp * create_glyphs(char *str, int line_height) {
  char * fontfile = "../ttf/dejavu.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;
  lc_bmp *bmp;

  if (!font) {
    perror("lc_create_font()");
    return NULL;
  }

  arr = lc_str_to_arr(font, str, line_height, 0);

  if (!arr) {
    perror("lc_str_to_arr()");
    return NULL;
  }

  bmp = lc_arr_to_bmp(arr);

  if (!bmp) {
    perror("lc_arr_to_bmp()");
    return NULL;
  }

  lc_free(font);
  lc_free(arr);
  return bmp;
}

lc_bmp * create_glyphs_pattern(const char *filename, int x, int y, int w, int h) {
  lc_bmp *img = lc_load_png(filename);
  lc_bmp *cropped;

  if (!img) {
    perror("lc_load_png()");
    return NULL;
  }

  cropped = lc_crop_bmp(img, x, y, w, h);

  if (!cropped) {
    perror("lc_crop_bmp()");
    return NULL;
  }

  lc_free(img);

  return cropped;
}

static int texturize_glyphs() {
  char * fap = "../patterns/96c.png";
  char * str = "(◕‿◕)funnyFACE";
  lc_bmp *text = create_glyphs(str, 30);
  lc_bmp *gpt;
  lc_bmp *gpat;

  if (!text) {
    return 1;
  }

  gpt = create_glyphs_pattern(fap, 200, 80, 230, 120);

  if (!gpt) {
    return 1;
  }

  gpat = lc_pattern_apply(gpt, text);

  if (!gpat) {
    perror("lc_pattern_apply()");
    return 1;
  }

/*  lc_save_png("/tmp/glyph.png", gpat);*/
/*  lc_save_png("/tmp/glyph.png", text);*/

  lc_free(text);
  lc_free(gpat);
  lc_free(gpt);
  return 0;
}

static int texturize_background() {
  char * fap = "../patterns/96b.png";
  char * fbp = "../patterns/96b.png";
  char * str = "LIBCAPTCHA";
  lc_bmp *text = create_glyphs(str, 30);
  lc_bmp *bg;
  lc_bmp *gpt;
  lc_bmp *gpat;
  lc_bmp *cp;

  if (!text) {
    return 1;
  }

  gpt = create_glyphs_pattern(fap, 400, 200, 200, 120);

  if (!gpt) {
    return 1;
  }

  bg = create_glyphs_pattern(fbp, 20, 180, 180, 60);

  if (!bg) {
    return 1;
  }

  gpat = lc_pattern_apply(gpt, text);

  if (!gpat) {
    perror("lc_pattern_apply()");
    return 1;
  }

  cp = lc_pattern_merge(bg, gpat, 24, 20);

/*  lc_save_png("/tmp/glyph.png", gpat);*/
/*  lc_save_png("/tmp/glyph.png", bg);*/
/*  lc_save_png("/tmp/glyph.png", cp);*/

  lc_free(cp);
  lc_free(text);
  lc_free(gpat);
  lc_free(bg);
  lc_free(gpt);
  return 0;
}

int main() {
  assert(!texturize_glyphs());
  assert(!texturize_background());
  return 0;
}
