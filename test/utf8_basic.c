/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>
#include <utf8.h>
#include <global.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <utf8.h>

static int create_utf8_bmp_glyph() {
  int sun = 0x2600;
  char *fontfile = "../ttf/dejavu.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_bmpGlyph *glyph;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  glyph = lc_create_glyph(font, sun, 320);

  if (!glyph) {
      puts("lc_create_glyph()");
      return 1;
  }

/*  stbi_write_png("/tmp/glyph.png", glyph->w, glyph->h, 1, glyph->buffer, glyph->w);*/

  lc_free(glyph);
  lc_free(font);

  return 0;
}

static int utf8_iter_test() {
   char * simpl = "😒 hello";
   int i = 0, counter = 0;
   uint32_t c;

   while((c = u8_nextchar(simpl, &i))) {
     counter++;
   }

   assert(counter == 7);
  return 0;
}

static int create_utf8_arr_glyph() {
  char * str = "😒test";
  char * fontfile = "../ttf/dejavu.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  arr = lc_str_to_arr(font, str, 38, 0);

  lc_free(arr);
  lc_free(font);

  return 0;
}

static int create_utf8_image() {

  return 0;
}

int main() {
  assert(!utf8_iter_test());
  assert(!create_utf8_bmp_glyph());
  assert(!create_utf8_arr_glyph());
  assert(!create_utf8_image());
  return 0;
}

