/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

static int create_bmp() {
  char * str = "😒 test WTF??";
  char * fontfile = "../ttf/dejavu.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;
  lc_bmp *bmp;


  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  arr = lc_str_to_arr(font, str, 50, 0);

  if (!arr) {
    perror("lc_str_to_arr()");
    return 1;
  }

  bmp = lc_arr_to_bmp(arr);

/*  stbi_write_png("/tmp/glyph.png", bmp->w, bmp->h, 1, bmp->buffer, bmp->w);*/

  lc_free(bmp);
  lc_free(arr);
  lc_free(font);

  return 0;
}

static int create_randomized_bmp() {
  char * str = "LIBCAPTCHA";
  char * fontfile = "../ttf/dejavu.ttf";
/*  char * fontfile = "../ttf/cmunrm/cmunrm.ttf";*/
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;
  lc_bmp *bmp;


  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  arr = lc_str_to_arr(font, str, 40, 70);

  if (!arr) {
    perror("lc_str_to_arr()");
    return 1;
  }

  lc_randomize_arr_rotation_180(arr);

  lc_randomize_arr_x(arr, 10);
  lc_randomize_arr_y(arr, 20);

  bmp = lc_arr_to_bmp(arr);

/*  lc_save_png("/tmp/out.png", bmp);*/

  lc_free(bmp);
  lc_free(arr);
  lc_free(font);

  return 0;
}

static int create_randomized_x() {
  char * str = "randomiz";
  char * fontfile = "../ttf/dejavu.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_arrGlyph *arr;
  lc_bmp *bmp;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  arr = lc_str_to_arr(font, str, 40, 0);

  if (!arr) {
    perror("lc_str_to_arr()");
    return 1;
  }

  lc_randomize_arr_x(arr, 30);
/*  lc_randomize_arr_y(arr, 40);*/

  bmp = lc_arr_to_bmp(arr);
/*  stbi_write_png("/tmp/glyph.png", bmp->w, bmp->h, 1, bmp->buffer, bmp->w);*/

  lc_free(bmp);
  lc_free(arr);
  lc_free(font);

  return 0;
}

int main() {
  assert(!create_bmp());
  assert(!create_randomized_bmp());
  assert(!create_randomized_x());
  return 0;
}

