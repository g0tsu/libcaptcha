/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <global.h>
#include <errno.h>

static int load_incorrect_file() {
  char *fontfile = "nofile.found";
  lc_fontBuffer *font = lc_create_font(fontfile);

  if (font != NULL) {
    return 1;
  }

  lc_free(font);
  return 0;
}

static int load_font_test() {
  char *fontfile = "../ttf/cmunrm/cmunrm.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  lc_free(font);

  return 0;
}

static int create_bmp_glyph() {
  char *fontfile = "../ttf/cmunrm/cmunrm.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_bmpGlyph *glyph;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  glyph = lc_create_glyph(font, 'g', 22);

  if (!glyph) {
      puts("lc_create_glyph()");
      return 1;
  }

  printf("glyph g is %dx%d\n", glyph->w, glyph->h);
/*  stbi_write_png("/tmp/glyph.png", glyph->w, glyph->h, 1, glyph->buffer, glyph->w);*/

  lc_free(glyph);
  lc_free(font);

  return 0;
}

static int random_stuff() {
  char bytes[4];

  lc_random_bytes(&bytes, 4);

  for (int i = 0; i < 4; i++) {
    printf("%x ", bytes[i] % 20);
  }

  return 0;
}

int main() {
  assert((2 + 2) == 4);
  assert(!load_incorrect_file());
  assert(!load_font_test());
  assert(!create_bmp_glyph());
  assert(!random_stuff());
  return 0;
}

