/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>
#include <global.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


static int create_shapes_pattern() {
  lc_bmp * text = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 50, 0, 10, 10);
  lc_bmp * fg = lc_generate_square_shapes(text->w, text->h, 2, 10);
  lc_bmp * bg = lc_generate_square_shapes(text->w, text->h, 2, 10);

  lc_colorize_sepia(bg);

  lc_bmp * cc = lc_pattern_apply(fg, text);
  lc_bmp * out = lc_pattern_merge(bg, cc, 0 , 0);

/*  lc_save_png("/tmp/out.png", out);*/

  lc_free(out);
  lc_free(cc);
  lc_free(bg);
  lc_free(fg);
  lc_free(text);
  return 0;
}

static int create_shapes_pattern_square() {
  lc_bmp * text = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 50, 0, 10, 10);
  lc_bmp * bmp = lc_preset_square(text, 2, 10);
/*  lc_save_png("/tmp/out.png", bmp);*/
  lc_free(bmp);
  lc_free(text);
  return 0;
}

static int create_shapes_pattern_circle() {
  lc_bmp * text = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 50, 0, 10, 10);
  lc_bmp * bmp = lc_preset_circle(text, 0, 14);
/*  lc_save_png("/tmp/out.png", bmp);*/
  lc_free(bmp);
  lc_free(text);
  return 0;
}

static int generate_screenshot() {
  lc_bmp * text = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 50, 0, 10, 10);
  lc_bmp * bmp = lc_create_3ch_bmp(text->w * 3 + 3, text->h * 2 + 2); //6 images
  int ow = text->w;
  int oh = text->h;

  lc_bmp * square = lc_preset_square(text, 1, 2);
  lc_place_bmp_shape(bmp, square, 0, 0);
  lc_free(square);

  square = lc_preset_square(text, 2, 15);
  lc_place_bmp_shape(bmp, square, text->w + 1, 0);
  lc_free(square);

  square = lc_preset_square(text, 2, 20);
  lc_place_bmp_shape(bmp, square, text->w * 2 + 1, 0);
  lc_free(square);

  lc_bmp * noise = lc_preset_noise(text);
  lc_place_bmp_shape(bmp, noise, 0, text->h + 1);
  lc_free(noise);

  square = lc_preset_circle(text, 0, 14);
  lc_place_bmp_shape(bmp, square, ow + 1, oh + 1);
  lc_free(square);

  square = lc_preset_circle(text, 0, 30);
  lc_place_bmp_shape(bmp, square, ow * 2 + 1, oh+ 1);
  lc_free(square);

/*  text = lc_preset_text("../ttf/dejavu.ttf", "MAYBEMORESOMEDAY😃️", 38, 50, 0, 10, 10);*/
/*  lc_bmp * fg = lc_generate_square_shapes(text->w, text->h, 2, 10);*/
/*  lc_bmp * cc = lc_pattern_apply(fg, text);*/

/*  lc_place_bmp_shape(bmp, cc, ow + 1, oh + 1);*/

/*  lc_save_png("/tmp/out.png", bmp);*/

/*  lc_free(fg);*/
/*  lc_free(cc);*/
  lc_free(bmp);
  lc_free(text);
  return 0;
}

int main() {
  assert(!create_shapes_pattern());
  assert(!create_shapes_pattern_square());
  assert(!create_shapes_pattern_circle());
  assert(!generate_screenshot());
  return 0;
}

