/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <global.h>

static int test_hex_to_rgb() {
  lc_rgb *rgb = lc_rgb_from_text("#ffbb0f00");

  if (rgb->r != 0xff || rgb->g != 0xbb || rgb->b != 0x0f) {
    lc_free(rgb);
    return 1;
  }

  lc_free(rgb);

  rgb = lc_rgb_from_text("#ffbb0f");

  if (rgb->r != 0xff || rgb->g != 0xbb || rgb->b != 0x0f) {
    lc_free(rgb);
    return 1;
  }

  lc_free(rgb);

  rgb = lc_rgb_from_text("ffbb0f");

  if (rgb)
    return 1;

  return 0;
}

int main() {
  assert(!test_hex_to_rgb());
  return 0;
}

