/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>
#include <global.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

static int create_obj() {
  lc_bmp *bmp = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 60, 1, 20, 20);

  if (!bmp) {
    perror("lc_reset_text()");
    return 1;
  }

/*  lc_save_png("/tmp/out.png", bmp);*/
  lc_free(bmp);

  return 0;
}

static int create_obj_noise() {
  lc_bmp *bmp = lc_preset_text("../ttf/dejavu.ttf", "HELLOWORLD", 38, 60, 0, 10, 10);
  lc_bmp *noise;

  if (!bmp) {
    perror("lc_reset_text()");
    return 1;
  }

  noise = lc_preset_noise(bmp);

  if (!noise) {
    lc_free(bmp);
    return 1;
  }

/*  lc_save_png("/tmp/out.png", noise);*/

  lc_free(bmp);
  lc_free(noise);
  return 0;
}

int main() {
  assert(!create_obj());
  assert(!create_obj_noise());
  return 0;
}

