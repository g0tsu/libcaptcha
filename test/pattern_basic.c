/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

static int load_png_bmp() {
  char * filename = "../patterns/96c.png";
  lc_bmp *img = lc_load_png(filename);

  if (!img) {
    perror("lc_load_png()");
    return 1;
  }

/*  stbi_write_png("/tmp/glyph.png", img->w, img->h,*/
/*     img->ch, img->buffer, img->w * img->ch);*/

  lc_free(img);

  return 0;
}

static int crop_png_bmp() {
  char * filename = "../patterns/96c.png";
  lc_bmp *img = lc_load_png(filename);
  lc_bmp *cropped;

  if (!img) {
    perror("lc_load_png()");
    return 1;
  }

  cropped = lc_crop_bmp(img, 100, 90, 400, 280);

  if (!cropped) {
    perror("lc_crop_bmp()");
    return 1;
  }

/*  stbi_write_png("/tmp/glyph.png", cropped->w, cropped->h,*/
/*     cropped->ch, cropped->buffer, cropped->w * cropped->ch);*/

  lc_free(cropped);
  lc_free(img);
  return 0;
}

int main() {
  assert(!load_png_bmp());
  assert(!crop_png_bmp());
  return 0;
}
