/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <assert.h>
#include <libcaptcha.h>
#include <errno.h>

static int create_bmp_glyph() {
  char *fontfile = "../ttf/cmunrm/cmunrm.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_bmpGlyph *glyph;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  glyph = lc_create_glyph(font, 'P', 320);

  if (!glyph) {
      puts("lc_create_glyph()");
      return 1;
  }

  lc_rotate_glyph_180(glyph);

/*  lc_save_png("/tmp/out.png", bmp);*/

  lc_free(glyph);
  lc_free(font);

  return 0;
}

static int rotate_bmp_180() {
  char *fontfile = "../ttf/cmunrm/cmunrm.ttf";
  lc_fontBuffer *font = lc_create_font(fontfile);
  lc_bmpGlyph *glyph;

  if (!font) {
    perror("lc_create_font()");
    return 1;
  }

  glyph = lc_create_glyph(font, 'P', 320);

  if (!glyph) {
      puts("lc_create_glyph()");
      return 1;
  }

  lc_rotate_glyph_180(glyph);

/*  lc_save_png("/tmp/out.png", bmp);*/

  lc_free(glyph);
  lc_free(font);
  return 0;
}

int main() {
  assert(!create_bmp_glyph());
  assert(!rotate_bmp_180());
  return 0;
}

