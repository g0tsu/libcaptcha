/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <global.h>
#include <libcaptcha.h>

/*
 * Create an empty, three channel bitmap
 */
lc_bmp * lc_create_3ch_bmp(int w, int h) {
  lc_bmp *bmp = malloc(sizeof(*bmp));

  if (!bmp) {
    errno = ENOMEM;
    return NULL;
  }

  bmp->buffer = calloc(1, 3 * (w * h));

  if (!bmp->buffer) {
    errno = ENOMEM;
    free(bmp);
    return NULL;
  }

  bmp->type = LC_TYPE_BMP;
  bmp->w = w;
  bmp->h = h;
  bmp->ch = 3;

  return bmp;
}

/*
 * Three channel bmp crop
 */
lc_bmp * lc_crop_bmp(lc_bmp *src, int xo, int yo, int w, int h) {
  lc_bmp *bmp =  lc_create_3ch_bmp(w, h);

  if (!bmp) {
    errno = ENOMEM;
    return NULL;
  }

  for (int sy = 0, y = yo; y < (yo + h); y++, sy++) {
    for (int xy = 0, xs = 0, x = xo; x < (xo + w); x++, xy++) {
      if (sy < bmp->h && xy < bmp->w && y < src->h && x < src->w) {
      *(bmp->buffer + (3 * (sy * w + xy)))      = *(src->buffer + (3 * (y * src->w + x)));
      *(bmp->buffer + (3 * (sy * w + xy)) + 1 ) = *(src->buffer + (3 * (y * src->w + x)) + 1);
      *(bmp->buffer + (3 * (sy * w + xy)) + 2 ) = *(src->buffer + (3 * (y * src->w + x)) + 2);
      }
    }
  }

  return bmp;
}

/*
 * Single channel merger
 */
void lc_merge_bmp(lc_bmp *dst, lc_bmp *src, int xo, int yo) {

  for (int y = 0; y < dst->h; y++) {
    for (int x = 0; x < dst->w; x++) {
      if (x < src->w && y < src->h && *(src->buffer + (y * src->w + x)) != 0) {
        int ym = y + yo;
        int xm = x + xo;

        *(dst->buffer + (ym * dst->w + xm)) = *(src->buffer + (y * src->w + x));
      }
    }
  }
}

/*
 * Single channel create
 */
lc_bmp * lc_create_bmp(const int width, const int height) {
  lc_bmp *bmp = malloc(sizeof(*bmp));

  if (!bmp) {
    errno = ENOMEM;
    return NULL;
  }

  bmp->buffer = calloc(sizeof(uint8_t), width * height);

  if (!bmp->buffer) {
    errno = ENOMEM;
    free(bmp);
    return NULL;
  }

  bmp->type = LC_TYPE_BMP;
  bmp->w = width;
  bmp->h = height;
  bmp->ch = 1;

  return bmp;
}

