/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#ifndef _L_LIBCAPTCHA_H
#define _L_LIBCAPTCHA_H

#include <stdint.h>
#include <stddef.h>
#include <math.h>

#ifndef u_uint32_t
typedef uint32_t u_int32_t;
#endif

/*
 * The buffer which holds TTF.
 */
typedef struct lc_fontBuffer {
  uint8_t type;
  uint8_t *buffer;
  void *info;
} lc_fontBuffer;

/*
 * Holds a corresponding chracter glyph,
 * single channel bmp with zeros (empty bits)
 * and 0xff bits which is a graphical representation
 * of the glyph.
 */
typedef struct lc_bmpGlyph {
  uint8_t type;
  uint8_t *buffer;
  int x;
  int y;
  int w;
  int h;
} lc_bmpGlyph;

/*
 * Array for glyphs.
 */
typedef struct lc_arrGlyph {
  uint8_t type;
  lc_bmpGlyph **items; // never do it at home
  int size;
  int w;
  int h;
} lc_arrGlyph;

/*
 * The great BMP
 */
typedef struct lc_bmp {
  uint8_t type;
  uint8_t *buffer;
  int w;
  int h;
  int x;
  int y;
  int ch;
} lc_bmp;

/*
 * A simplified part of API
 */

/*
 * lc_free() the most important function, it
 * frees any object libcaptcha returns.
 */
void lc_free(void *obj);

/*
 * lc_save_png() simply writes any bitmap into filename.
 * lc_load_png() craates lc_bmp from file.
 */
int lc_save_png(const char *filename, lc_bmp *bmp);
lc_bmp * lc_load_png(const char *filename);

/*
 * preset0 (level zero)
 *
 * lc_preset_text() creates mask for the specified font and text
 * with an addditional randomization options.
 */
lc_bmp * lc_preset_text(const char *font_filename, const char *text,
    const int font_size, const int font_randsizeto,
    const unsigned char font_randrotate,
    const int glyph_rand_x, const int glyph_rand_y);

/*
 * Colorization presets
 */
lc_bmp *lc_preset_noise(lc_bmp *src);
lc_bmp *lc_preset_square(lc_bmp *src, int space, int size);
lc_bmp *lc_preset_circle(lc_bmp *src, int space, int radius);

/*
 *  Primitives
 */

/*
 * lc_create_font() function creates font object from file.
 * lc_str_to_arr() string into array of glyphs using font
 * lc_arr_to_bmp() creates 1ch bitmap from array
 */
lc_fontBuffer * lc_create_font(const char *filename);

lc_arrGlyph * lc_str_to_arr(lc_fontBuffer * font, const char *str,
    const int line_height, const int delta);

lc_bmp * lc_arr_to_bmp(lc_arrGlyph *arr);

/*
 * Array randomizers/mixers
 */

void lc_randomize_arr_x(lc_arrGlyph *arr, int delta);
void lc_randomize_arr_y(lc_arrGlyph *arr, int delta);
void lc_randomize_arr_rotation_180(lc_arrGlyph *arr);

void lc_rotate_glyph_180(lc_bmpGlyph *glyph);

/*
 * 8-bit bmp functions
 */
lc_bmp * lc_create_bmp(const int width, const int height);
lc_bmp *lc_rotate_bmp_180(uint8_t *buffer, int width, int height);
void lc_merge_3ch_bmp(lc_bmp *dst, lc_bmp *src, int xo, int yo);

/*
 * 24-bit bmp functions
 */
lc_bmp * lc_create_3ch_bmp(int w, int h);
lc_bmp * lc_crop_bmp(lc_bmp *src, int x, int y, int w, int h);
void lc_merge_bmp_shape(lc_bmp *bmp, lc_bmp *shape, int x, int y);

/*
 * Applies 24-bit pattern to 8-bit bitmap
 */
lc_bmp * lc_pattern_apply(lc_bmp *pattern, lc_bmp *dst);

/*
 * Merges two 24-bit bitmaps using x,y offset
 */
lc_bmp * lc_pattern_merge(lc_bmp *bg, lc_bmp *bmp, int x, int y);

#endif

