/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libcaptcha.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stb_truetype.h>
#include <global.h>

static void lc_place_glyph(lc_bmp *bmp, lc_bmpGlyph *glyph) {
  int xoff = glyph->x;
  int yoff = glyph->y;

  for (int y = 0; y < glyph->h; y++) {
    if (y >= bmp->h)
      break;
    for (int x = 0; x < glyph->w; x++) {
      if (x >= bmp->w)
        break;
      int c = *(glyph->buffer + (x + y * glyph->w));
      if (c != 0) {
        *(bmp->buffer + ((xoff + x) + (yoff + y) * bmp->w)) = c;
      }
    }
  }
}

/*
 * Creates a font object from TTF file
 */
lc_fontBuffer * lc_create_font(const char *filename) {
  FILE *font_fd = fopen(filename, "rb");
  size_t font_size = 0;
  lc_fontBuffer *font;
  stbtt_fontinfo *info;

  if (font_fd == NULL) {
    return NULL;
  }

  fseek(font_fd, 0, SEEK_END);
  font_size = ftell(font_fd);
  fseek(font_fd, 0, SEEK_SET);

  if (font_size < 8) {
    return NULL;
  }

  if ((info = malloc(sizeof(*info))) == NULL) {
    errno = ENOMEM;
    return NULL;
  }

  if ((font = malloc(sizeof(*font))) == NULL) {
    errno = ENOMEM;
    return NULL;
  }

  if ((font->buffer = malloc(font_size)) == NULL) {
    errno = ENOMEM;
    free(info);
    free(font);
    return NULL;
  }

  fread(font->buffer, font_size, 1, font_fd);
  fclose(font_fd);

  font->type = LC_TYPE_FONT;
  font->info = info;

  if (!stbtt_InitFont(font->info, font->buffer, 0)) {
    errno = EINVAL;
    lc_free(font);
    return NULL;
  }

  return font;
}

lc_bmpGlyph * lc_create_glyph(lc_fontBuffer *font, const int glyph, const int line_height) {
  int w, h;
  float scale;
  unsigned char *bitmap;
  lc_bmpGlyph *bg;

  if ((bg = malloc(sizeof(*bg))) == NULL) {
    errno = ENOMEM;
    return NULL;
  }

  scale = stbtt_ScaleForPixelHeight(font->info, line_height);
  bg->buffer = stbtt_GetCodepointBitmap(font->info, 0, scale, glyph, &w, &h, 0, 0);
  bg->type = LC_TYPE_GLYPH;
  bg->w = w;
  bg->h = h;

  return bg;
}

/*
 * Convert array of glyphs
 * into a single channel bitmap
 */
lc_bmp * lc_arr_to_bmp(lc_arrGlyph *arr) {
  lc_bmpGlyph *item;
  lc_bmp * bmp = lc_create_bmp(arr->w, arr->h);

  if (!bmp) {
    errno = ENOMEM;
    return NULL;
  }

  bmp->type = LC_TYPE_BMP;
  bmp->ch = 1;
  bmp->w = arr->w;
  bmp->h = arr->h;

  for (int i = 0; i < arr->size; i++) {
    item = arr->items[i];
    lc_place_glyph(bmp, item);
  }

  return bmp;
}

