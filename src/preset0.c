/*
 *  this program is free software; you can redistribute it and/or
 *  modify it under the terms of the gnu general public license
 *  as published by the free software foundation; either version 2
 *  of the license, or (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but without any warranty; without even the implied warranty of
 *  merchantability or fitness for a particular purpose.  see the
 *  gnu general public license for more details.
 *  author: g0tsu
 *  email:  g0tsu at dnmx.0rg
 */

#include <libcaptcha.h>
#include <stdio.h>
#include <stdint.h>
#include <global.h>

lc_bmp * lc_preset_text(const char *font_filename, const char *text,
    const int font_size, const int font_randsizeto,
    const unsigned char font_randrotate,
    const int glyph_rand_x, const int glyph_rand_y) {

  lc_fontBuffer *font = lc_create_font(font_filename);
  lc_arrGlyph *arr;
  lc_bmp *text_bmp;
  lc_bmp *bmp;

  if (!font)
    return NULL;

  arr = lc_str_to_arr(font, text, font_size, font_randsizeto);

  lc_free(font);

  if (!arr)
    return NULL;

  if (glyph_rand_x > 0)
    lc_randomize_arr_x(arr, glyph_rand_x);

  if (glyph_rand_y > 0)
    lc_randomize_arr_y(arr, glyph_rand_y);

  if (font_randrotate)
    lc_randomize_arr_rotation_180(arr);

  text_bmp = lc_arr_to_bmp(arr);

  lc_free(arr);

  if (!text_bmp)
    return NULL;

  bmp = lc_create_bmp(text_bmp->w + _LC_DEFAULT_HPADDING * 2,
                      text_bmp->h + _LC_DEFAULT_VPADDING * 2);

  if (!bmp)
    return NULL;

  lc_merge_bmp(bmp, text_bmp, _LC_DEFAULT_HPADDING, _LC_DEFAULT_VPADDING);

  lc_free(text_bmp);

  return bmp;
}

lc_bmp *lc_preset_noise(lc_bmp *src) {
  lc_bmp *bmp = lc_generate_noise(src, src->w, src->h);
  lc_bmp *pmp = lc_generate_noise(NULL, src->w, src->h);
  lc_bmp *out = lc_pattern_merge(pmp, bmp, 0, 0);

  lc_free(bmp);
  lc_free(pmp);

  return out;
}

lc_bmp *lc_preset_square(lc_bmp *src, int space, int size) {
  lc_bmp * fg = lc_generate_square_shapes(src->w, src->h, space, size);
  lc_bmp * bg = lc_generate_square_shapes(src->w, src->h, space, size);

  lc_colorize_sepia(bg);

  lc_bmp * tfg = lc_pattern_apply(fg, src);
  lc_bmp * out = lc_pattern_merge(bg, tfg, 0 , 0);

  lc_free(tfg);
  lc_free(bg);
  lc_free(fg);

  return out;
}

lc_bmp *lc_preset_circle(lc_bmp *src, int space, int radius) {
  lc_bmp * fg0 = lc_generate_circle_shapes(src->w, src->h, space, radius);
  lc_bmp * fg1 = lc_generate_circle_shapes(
      src->w - radius /2, src->h - radius /2, space, radius);

  lc_bmp * fg = lc_pattern_merge(fg0, fg1, radius / 2, radius /2);

  lc_free(fg0);
  lc_free(fg1);

  lc_bmp * bg0 = lc_generate_circle_shapes(src->w, src->h, space, radius);
  lc_bmp * bg1 = lc_generate_circle_shapes(
      src->w - radius/2, src->h - radius/2, space, radius);

  lc_bmp * bg = lc_pattern_merge(bg0, bg1, radius / 2, radius /2);

  lc_free(bg0);
  lc_free(bg1);

  lc_colorize_sepia(bg);

  lc_bmp * tfg = lc_pattern_apply(fg, src);
  lc_bmp * out = lc_pattern_merge(bg, tfg, 0, 0);

  lc_free(tfg);
  lc_free(bg);
  lc_free(fg);

  return out;
}

