/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libcaptcha.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <global.h>

/*
 * Single channel bmp rotation
 */
lc_bmp *lc_rotate_bmp_180(uint8_t *buffer, int width, int height) {
  lc_bmp *dst = lc_create_bmp(width, height);

  for (int y = 0, p = dst->h - 1; y < dst->h; y++, p--) {
    for (int x = 0; x < dst->w; x++) {
      if (*(buffer + (y * width + x)) != 0) {
        *(dst->buffer + (p * dst->w + x)) = *(buffer + (y * width + x));
      }
    }
  }

  return dst;
}

/*
 * Rotate glyph 180
 */


void lc_rotate_glyph_180(lc_bmpGlyph *glyph) {
  lc_rotate_bmp_180(glyph->buffer, glyph->w, glyph->h);
}

