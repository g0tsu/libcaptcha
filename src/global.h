/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#ifndef __LC_GLOBAL_H
#define __LC_GLOBAL_H

#include <stddef.h>
#include <libcaptcha.h>

#define MAX(a,b) (((a)>(b))?(a):(b))

#define _LC_DEFAULT_VPADDING 15
#define _LC_DEFAULT_HPADDING 25

#define PI 3.14151592654

typedef struct lc_rgb {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} lc_rgb;

enum LC_TYPES {
  LC_TYPE_FONT = 0,
  LC_TYPE_GLYPH,
  LC_TYPE_ARR,
  LC_TYPE_BMP
};

/*
 * font
 */
lc_bmpGlyph * lc_create_glyph(lc_fontBuffer *font, const int glyph,
    const int line_height);

/*
 * image
 */


/*
 * generator1
 */
void lc_recalculate_arr(lc_arrGlyph *arr);

/*
 * pattern-gen
 */
void lc_place_bmp_shape(lc_bmp *bmp, lc_bmp *shape, int x, int y);

lc_bmp * lc_generate_square_shapes(int w, int h, int space, int step);
lc_bmp * lc_generate_circle_shapes(int w, int h, int space, int radius);

lc_bmp * lc_generate_noise(lc_bmp *pattern, int w, int h);
lc_bmp *lc_generate_square(int w, int h);
lc_bmp *lc_generate_circle(int r);

void lc_colorize_sepia(lc_bmp *bmp);

/*
 * utils
 */
int lc_random_bytes(void *bytes, size_t size);
lc_rgb *lc_rgb_from_text(const char *str);

#endif

