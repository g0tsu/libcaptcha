/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <config.h>
#include <stdio.h>
#include <libcaptcha.h>

#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#define STBI_NO_PNM
#define STBI_NO_PSD
#define STBI_NO_BMP
#define STBI_NO_GIF
#define STBI_NO_PIC
#define STBI_NO_TGA
/*#define STBI_NO_HDR*/
/*#define STBI_NO_JPEG*/
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <errno.h>
#include <global.h>

typedef struct lc_objtf {
  uint8_t type;
  void *field1;
  void *field2;
} lc_objtf;

static void lc_free_arr(lc_arrGlyph *arr) {
  for (int i = 0; i < arr->size; i++) {
    lc_free(arr->items[i]);
  }

  free(arr->items);
  free(arr);
}

/*
 * Saves BMP into file
 */
int lc_save_png(const char *filename, lc_bmp *bmp) {
  return stbi_write_png(filename, bmp->w, bmp->h,
     bmp->ch, bmp->buffer, bmp->w * bmp->ch);
}

/*
 * Loads BMP from file
 */
lc_bmp * lc_load_png(const char *filename) {
  uint8_t *tmp;
  lc_bmp *img = lc_create_3ch_bmp(1,1);

  if (!img) {
    errno = ENOMEM;
    return NULL;
  }

  free(img->buffer);

  /*
   * TODO: a better file checks
   */
  img->buffer = stbi_load(filename, &img->w, &img->h, &img->ch, 3);

  if (!img->buffer) {
    errno = ENOENT;
    free(img);
    return NULL;
  }

  return img;
}

/*
 * Super free()
 */
void lc_free(void *obj) {
  lc_objtf *o = obj;
  if (o != NULL) {
    if (o->type == LC_TYPE_ARR) {
      lc_free_arr((lc_arrGlyph *)obj);
      return;
    }

    if (o->field1) {
      free(o->field1);
    }

    if (o->type == LC_TYPE_FONT &&
        o->field2) {
      free(o->field2);
    }

    free(o);
  }
}

