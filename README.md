### Libcaptcha

> Highly customizable `captcha` making library.

![screen](examples/example7.png)
The screenshot was made using [example7.c](examples/example7.c)

## Table of contents

  * [Introduction](#introduction)
  * [Features](#features)
  * [How to install](#how-to-install)
  * [Principles](#principles)
  * [How to use](#how-to-use)
  * [Advanced use (examples)](#advanced-use)
  * [License](#license)

## Introduction

The main idea was to make a general purpose graphic library with no dependencies
in `C` to draw some primitives with text in a web application. And then I've noticed
that I don't have a good `captcha` to prevent robots to post their shit, so, now
here it is.

## Features

  - TTF font support
  - UTF-8 support, you can use any glyph
  - An easy API (I suppose)
  - Zero dependencies
  - Entirely written in `C`
  - Can be used as a `single-header` library (thanks to @acetone)

## How to install

To create a `single-header` library just use:

```
./make-singleh.sh
```

If command succeed, you'll have the `libcaptcha.h` file that you can use as
a single header library.

It is as simple as

```
./bootstrap
./configure
make
make install
```

## Principles

I thought to name this section "limitations", because `libcaptcha` doesn't
provide functionality to align fonts, work with alpha channels and so on. But
`captcha` does not need those "features". Rather vice versa - it requires to be ugly
and random.

The general idea is to provide a simplified `API` (I call it level zero or preset0)
and more advanced primitives.

The simplified functionality consists of 3 simple steps:

  * Create a font object (with text and other options)
  * Render something (using pattern generators)
  * Save file

The advanced functions are quite low level stuff, that requires some understaindig
of every step. I've provided some [examples](examples) which indended to help you with
understanding those "low level" functions. By you I mean me in the future :)

## How to use

First of all we need some text, and this text should be placed using
some random values. Don't warry, `preset` functions make all those
random stuff by itself.

Here is example how to use `lc_preset_text()` :

```
  lc_bmp * text = lc_preset_text( // returns single channel lc_bmp*
    "path_to_font.ttf",           // path to font file
    "HELLOWORLD",                 // our text
    38,                           // minimum font size
    50,                           // maximum font size (if zero, all glyphs will be 38)
    1,                            // 1 - rotate characters, 0 - off
    10,                           // horizontal random offset
    10);                          // vertical random offset
 ```


There's some simply functions that make `noise` and `square` captchas.

```
  lc_bmp * bmp = lc_preset_square(
    text,                         // is our font object with text
    1,                            // spacing
    2);                           // square size (2 is very small)


```

For circles:

```
  lc_bmp * bmp = lc_preset_circle(
    text,                         // is our font object with text
    0,                            // spacing
    14);                          // circle radius (it is not a real radius, but close to it)


```

Lets save our image into a `png` file:

```
 lc_save_png("helloworld.png", bmp);

```

Don't forget to `free()` memory:

```
  lc_free(bmp);
  lc_free(text);
```

Also you can make some noisy pattern instead of squares, its usage is simple:

```
  lc_bmp * noise = lc_preset_noise(text);

```

That's it.


## Advanced Use

Check out [examples](examples/)

Get familiar with general concept. [example1.c](examples/example1.c)

![example1](examples/example1.png)

How to make a random sized glyphs. [example2.c](examples/example2.c)

![example2](examples/example2.png)

Apply random positions for glyphs. [example3.c](examples/example3.c)

![example3](examples/example3.png)

Get familiar with patterns made from png images. [example4.c](examples/example4.c)

![example4](examples/example4.png)

Patterns, patterns everywhere. [example5.c](examples/example5.c)

![example5](examples/example5.png)

Some crazy stuff. [example6.c](examples/example6.c)

![example6](examples/example6.png)

Don't forget to use `lc_free()` to free all the stuff `libcaptcha` returns to 
you.

I've noticed that it is very easy to get lost sometimes, just use `valgrind`.

## License

GPLv2

```
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  Author: g0tsu
  Email:  g0tsu at dnmx.0rg
```

